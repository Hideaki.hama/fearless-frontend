window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/locations/";
  // EventListner when the DOM loads
  // Declare url as variable for the API /states/

  const response = await fetch(url);

  if (response.ok) {
    const data = await response.json();

    const selectTag = document.getElementById("location");
    for (let location of data.locations) {
      let option = document.createElement("option");

      option.value = location.pk;
      option.innerHTML = location.name;

      selectTag.appendChild(option);
    }
  }

  const formTag = document.getElementById("create-conference-form");
  formTag.addEventListener("submit", async (event) => {
    event.preventDefault();
    const name = document.getElementById("name").value;
    const starts = document.getElementById("starts").value;
    const ends = document.getElementById("ends").value;
    const description = document.getElementById("description").value;
    const max_presentations =
      document.getElementById("max_presentations").value;
    const max_attendees = document.getElementById("max_attendees").value;

    const location = Number.parseInt(document.getElementById("location").value);

    const data = {
      name: name,
      starts: starts,
      ends: ends,
      description: description,
      max_presentations: max_presentations,
      max_attendees: max_attendees,
      location: location,
    };

    const conferencesUrl = "http://localhost:8000/api/conferences/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(conferencesUrl, fetchConfig);
    if (response.ok) {
      formTag.reset();
      const newConference = await response.json();
    }
  });
});
