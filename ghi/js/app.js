window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

  function createCard(title, description, pictureUrl, starts, ends, location) {
    return `

    <div  class="shadow m-3" >

        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <p class = "card-subtitle mb-2 text-muted">${location}</p>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
      ${starts} - ${ends}
      </div>
    </div>

    `;
  }

  function err() {
    return `

    <div class="alert alert-danger" role="alert">
        A simple danger alert—check it out!
    </div>

    `;
  }

  try {
    const response = await fetch(url);

    if (!response.ok) {
      throw new Error("Response not ok");
    } else {
      const data = await response.json();
      let index = 0;
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();

          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;

          const startDate = details.conference.starts;
          const getStartDate = new Date(startDate);
          const starts = getStartDate.toLocaleDateString();
          // const starts = new Date(details.conference.starts).toLocaleDatestring()

          const endDate = details.conference.ends;
          const getEndDate = new Date(endDate);
          const ends = getEndDate.toLocaleDateString();

          const location = details.conference.location.name;

          const html = createCard(
            title,
            description,
            pictureUrl,
            starts,
            ends,
            location
          );
          const column = document.querySelector(`#col-${index % 3}`);
          column.innerHTML += html;
          index += 1;
        }
      }
    }
  } catch (e) {
    console.error("Response not ok"); // in console

    const errhtml = err();
    const alert = document.querySelector(".throw-error");
    alert.innerHTML = errhtml;
  }
});
