// import React, { useEffect, useState} from 'react'


// function PresentationForm () {

//   const [name, setName] = useState('')
//   const [email, setEmail] = useState('')
//   const [companyName, setCompanyName] = useState('')
//   const [title, setTitle] = useState('')
//   const [synopsis, setSynopsis] = useState('')
//   const [conference, setConference] = useState('')
//   const [conferences, setConferences] = useState([])

//   useEffect(() => {
//     const fetchConferences = async () => {
//       const conferenceUrl = `http://localhost:8000/api/conferences/`
//       const response = await fetch(conferenceUrl)
//       if (response.ok) {
//         const data = await response.json()
//         setConferences(data.conferences)
//       }
//     }

//     fetchConferences()
//   } , [])


//     return (
//       <div className="row">
//         <div className="offset-3 col-6">
//           <div className="shadow p-4 mt-4">
//             <h1>Create a new presentation</h1>
//             <form id="create-presentation-form">
//               <div className="form-floating mb-3">
//                 <input onChange={(event) => setName(event.target.value)} value={name} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" />
//                 <label htmlFor="presenter_name">Presenter name</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input onChange={(event) => setEmail(event.target.value)} value={email}placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control" />
//                 <label htmlFor="presenter_email">Presenter email</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input onChange={(event) => setCompanyName(event.target.value)}value={companyName}placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control" />
//                 <label htmlFor="company_name">Company name</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input onChange={(event) => setTitle(event.target.value)} value={title}placeholder="Title" required type="text" name="title" id="title" className="form-control" />
//                 <label htmlFor="title">Title</label>
//               </div>
//               <div className="mb-3">
//                 <label htmlFor="synopsis">Synopsis</label>
//                 <textarea onChange={(event) => setSynopsis(event.target.value)} value={synopsis} className="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
//               </div>
//               <div className="mb-3">
//                 <select onChange={(event) => setConference(event.target.value)} value={conference} required name="conference" id="conference" className="form-select">
//                   <option value="">Choose a conference</option>
//                   {conferences.map(conference =>{
//                     return(
//                       <option key={conference.href}  value={conference.href} >
//                         {conference.name}
//                       </option>
//                       )
//                   })}
//                 </select>
//               </div>
//               <button className="btn btn-primary">Create</button>
//             </form>
//           </div>
//         </div>
//       </div>
//     );
//   }


// export default PresentationForm;